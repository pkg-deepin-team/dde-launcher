#define private public
#include "menuworker.h"
#undef private

#include <QTest>
#include <QMenu>

#include <gtest/gtest.h>

class Tst_MenuWorker : public testing::Test
{};

TEST_F(Tst_MenuWorker, menuWorker_test)
{
    MenuWorker worker;
    QMenu menu;
    menu.setAccessibleName("popmenu");

    QSignalMapper *signalMapper = new QSignalMapper(&menu);
    worker.creatMenuByAppItem(&menu, signalMapper);

    for (int index = 0; index < 7; index++)
        signalMapper->mapped(1);

    menu.aboutToHide();

    const QModelIndex index;
    worker.setCurrentModelIndex(index);
    QVERIFY(index == worker.getCurrentModelIndex());
}
